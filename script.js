let trainer = {

	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Greninja", "Charizard"],
	friends:{
		kanto:["Brock", "Misty"],
		hoenn:["May", "Max"]
	},
	talk: function(pokemon){
	console.log(`${pokemon} I choose you!`)
	}
}

console.log(trainer)

console.log(`Result of dot notation:`)

console.log(trainer.name)

console.log(`Result of bracket notation:`)

console.log(trainer["pokemon"])

console.log(`Result of talk method:`)

trainer.talk(trainer.pokemon[0])


function Pokemon(name, lvl){
	this.name = name;
	this.level = lvl;
	this.health = lvl*5;
	this.attack = parseFloat((lvl*1.5).toFixed(0));
	this.tackle = function(opponent){
		opponent.health -= this.attack;
		console.log(`${this.name} tackled ${opponent.name}`)
		if (opponent.health > 0){
			console.log(`${opponent.name} has ${opponent.health}hp`)
		}
		else{
			console.log(`${opponent.name} fainted`)}
		}	

}

		

let pikachu = new Pokemon("Pikachu", 5)
let ratata = new Pokemon("Ratata", 3)
let bidoof = new Pokemon("Bidoof", 99)
console.log(pikachu)
console.log(ratata)
console.log(bidoof)

pikachu.tackle(bidoof)

pikachu.tackle(ratata)
pikachu.tackle(ratata)


console.log(ratata)














